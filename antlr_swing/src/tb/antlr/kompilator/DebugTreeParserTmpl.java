/**
 * 
 */
package tb.antlr.kompilator;

import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.debug.DebugEventListener;
import org.antlr.runtime.debug.DebugTreeParser;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;

import tb.antlr.symbolTable.GlobalSymbols;

/**
 * @author tb
 *
 */
public class DebugTreeParserTmpl extends DebugTreeParser {

	protected GlobalSymbols globals = new GlobalSymbols();
	
	/**
	 * @param input
	 * @param recognizerSharedState 
	 */
	public DebugTreeParserTmpl(TreeNodeStream input, DebugEventListener dbg, RecognizerSharedState recognizerSharedState) {
		super(input, dbg);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param input
	 * @param state
	 */
	public DebugTreeParserTmpl(TreeNodeStream input, RecognizerSharedState state) {
		super(input, state);
		// TODO Auto-generated constructor stub
	}

	protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}

}
