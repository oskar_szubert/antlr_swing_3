	grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog: 
    (stat | blok )+ EOF!
    ;

blok:
      BEGIN^ (stat | blok)* END!
    ;

stat
    : expr NL -> expr

    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
//    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
//    | if_stat NL -> if_stat // zostawiam, bo  dziala zwykle if else(zgodnie z filmem na yt), istnieje prawidłowa obsluga w pliku TExpr3.g oraz pierwszy.stg 
    | if_elif_stat NL -> if_elif_stat
    | NL ->
    ;

if_stat
    : IF^ expr THEN! (expr) (ELSE! (expr))?
//    : IF^ expr THEN! (blok|expr) (ELSE! (blok|expr))?
    ;
if_elif_stat
    : IF^ expr THEN!(expr) (ELIF (expr) THEN! (expr))? (ELSE! (expr))?
;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

IF  : 'if';

ELSE :'else';

ELIF : 'elif';

THEN :'then'; 

BEGIN : '{';

END : '}';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t' | ';')+ {$channel = HIDDEN;} ; // jak w pythonie, znak ; jest ignorowany na koncu lini


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
